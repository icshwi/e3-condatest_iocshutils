# require statments should only specify the module name
# The version of the module shall be defined in the environment.yaml file
# Note that all modules shall be in lowercase
## Add extra modules here
require iocshutils

# Set EPICS environment variables
## Edit these as required for specific IOC requirements
## General IOC environment variables
#epicsEnvSet("PREFIX", "EDIT: change this")
#epicsEnvSet("DEVICE", "EDIT: change this")
#epicsEnvSet("LOCATION", "EDIT: change this")
#epicsEnvSet("ENGINEER", "EDIT: Name <email@esss.se>"
## Add extra environment variables here

# Load standard module startup scripts
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

## Add extra startup scripts requirements here
# iocshLoad("$(module_DIR)/module.iocsh", "MACRO=MACRO_VALUE")

## Load custom databases
cd $(E3_IOCSH_TOP)
dbLoadRecords("db/bpt20klDewar.dbd")
updateMenuConvert
dbLoadRecords("db/t.db")

#dbLoadTemplate("db/custom_database2.substitutions", "MACRO1=MACRO1_VALUE,...")

# Call iocInit to start the IOC
iocInit()

## Add any post-iocInit statements here

